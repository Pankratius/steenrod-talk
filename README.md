These are some notes for my talk on Steenrod operations, in a [Seminar run by Prof. Carl-Friedrich Bödigheimer][1] in summer 2021 at Bonn University.

[1]: https://www.math.uni-bonn.de/~cfb/Seminar-SS-2021.pdf
