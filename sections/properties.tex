\section{Properties of the Steenrod squares}
\begin{nonumconvention}
  Let $m,n\in \zz$.
  Then we set
  \[
  \binom{n}{m}
  \defined
  \begin{cases}
    \frac{n\cdot(n-1)\cdot\ldots\cdot(n-m+1)}{m\cdot(m-1)\cdot \ldots \cdot 1}
    &
    \text{if }m\geq 1,
    \\
    1
    &
    \text{if }m = 0,
    \\
    0
    &
    \text{otherwise.}
  \end{cases}
  \]
\end{nonumconvention}
\subsection{Cartan Formular}
\begin{construction}
  Let $V$ and $W$ be complexes.
  We want to construct a map
  \[
  \psi\mc D_2(V\tensor W)\to D_2(V)\tensor D_2(W)
  \]
  For that, we observe that we have
  \begin{align*}
    D_2(V)\tensor D_2(W)
    &
    \cong
    V^{\tensor 2}\hci\tensor W^{\tensor 2}\hci
    \cong
    \left(V\tensor W\right)^{\tensor 2}_{\mathrm{h}(G\times G)}
    \intertext{and}
    D_2(V\tensor W)
    &
    \cong
    (V\tensor W)^{\tensor 2}\hci.
  \end{align*}
  Now the diagonal embedding $G\hookrightarrow G\times G$ induces the map $\psi$ that we want.
\end{construction}
\begin{lem}
  Let $V$ and $W$ be complexes, $v\in \hlgy_m(V)$ and $w\in \hlgy_n(W)$.
  Then for every $k\in \zz$, we have an equality
  \[
  \psi
  \left(
  \overline{\sq}^k(v\tensor w)
  \right)
  =
  \sum_{k=i+j}\overline{\sq}^i(v)\tensor \overline{\sq}^j(w)
  \]
  in the homology group $\hlgy_{m+n+k}(D_2(V)\tensor D_2(W))$.
\end{lem}
\begin{proof}
  We again exploit that both $V$ and $W$ are complexes of $\fz$-modules, so without loss of generality, we can assume that $V = \fz[-m]$ and $W = \fz[-n]$.
  Using \cref{def-operations:dtwo-homology}, we have
  \begin{align*}
    \hlgy_{\bullet}(D_2(V))
    &
    \cong
    \bigoplus_{i\in \zz} (\fz\cdot x_{2m-i})[i]
    \\
    \hlgy_{\bullet}(D_2(W))
    &
    \cong
    \bigoplus_{i\in \zz}(\fz\cdot x_{2n-i})[i]
    \\
    \hlgy_{\bullet}(D_2(V\tensor W))
    &
    \cong
    \bigoplus_{i\in \zz}(\fz\cdot x_{2(m+n)})[i],
  \end{align*}
  where we set $x_i = 0$ for $i<0$.
  If we denote the generator of $V$ by $v$ and the generator of $W$ by $w$, then we have
  \[
    \osq^{m+n-i}(v\tensor w)
    x_i,
  \]
  and similarly for $v$ and $w$.
  Since we are working over a field, we have a comultiplication
  \[
  \Psi
  \mc
  \hlgy_{\bullet}^{\mrm{sing}}(\rpi,\fz)
  \to
  \hlgy_{\bullet}^{\mrm{sing}}(\rpi,\fz)\tensor\hlgy_{\bullet}^{\mrm{sing}}(\rpi,\fz)
  \]
  induced by the Alexander-Whitney map (\cref{app:induction:aw-comultiplication}), which is on basis vectors given by
  \[
  x_i \mapsto \sum_{i'+i''= i}x_{i'}\tensor x_{i''}.
  \]
  The claim follows since under the identification of $\hlgy_{\bullet}(D_2(V))$ with the singular homology of $\rpi$, the map $\psi$ corresponds to the map $\Psi$.
\end{proof}
\begin{numtext}
  We would like to extend this result to topological spaces.
  This turns out to be possible, and will go by the name \emph{Cartan formular}.
  Getting there requires us to introduce and then prove one more technical condition describing the coherence of the $D_2$-functor.
  To begin, note that we have a canonical map
  \[
  \pphi\mc D_2(D_2(V))
  \to
  D_{4}(V)
  \]
  induced by the inclusion \[G^2\rtimes G \hookrightarrow \mathfrak{S}_4,\] where we denote
  \[
  D_4(V)\defined (V^{\tensor 4}\tensor \mrm{E}\ff_4)_{\mathfrak{S}_4}.
  \]
  We say that a symmetric multiplication $m\mc D_2(V)\to V$ is \emph{$2$-compatible} if there is a map $m'\mc D_4(V)\to V$ such that the diagram
  \[
  \begin{tikzcd}
    D_2(D_2(V))
    \ar{r}[above]{D_2(m)}
    \ar{d}[left]{\pphi}
    &
    D_2(V)
    \ar{d}[right]{m}
    \\
    D_4(V)
    \ar{r}[below]{m'}
    &
    V
  \end{tikzcd}
  \]
  commutes.
\end{numtext}
\begin{prop}
  Let $Y$ be a topological space.
  Then the symmetric multiplication on $\singcochain(Y;\fz)$ from is $2$-compatible.
\end{prop}
\begin{prop}
  Let $V$ be a complex with a $2$-compatible symmetric multiplication.
  Then for every $v,w\in \hlgy_{\bullet}(V)$ the \emph{Cartan formular} holds:
  \[
  \sq^k(vw) = \sum_{k=k'+k''}\sq^{k'}(v)\sq^{k''}(w).
  \]
\end{prop}
\subsection{Adem relations}
\begin{prop}\label{properties:adem-relations}
  Let $V$ be a complex equipped with a $2$-compatible symmetric multiplication, and let $v\in \hlgy_n(V)$.
  Then for any pair of integers $a<2b$, we have
  \[
  \sq^a(v)\sq^b(v)
  =
  \sum_{k}\binom{2k-a}{b-k-1}\sq^{b+k}\sq^{a-k}(v).
  \]
  These relations are called \emph{Adem relations}.
\end{prop}
Proving this is a technical nightmare, so we will not carry it out in all detail.
A combinatorial argument together with clever application of the shift functor shows that it suffices to show the following more symmetric statement:
\begin{lem}
  Let $p$ and $q$ be positive integers, let $V$ be a complex and let $v\in\hlgy_n(V)$.
  Then the sums
  \begin{align*}
  \sum_l \binom{p-2l}{l}\osq^{2n-q-l}\osq^{n-p+l}(v)
  &
  \in \hlgy_{4n-p-q}(D_2(D_2))
  \\
  \sum_{l'}\binom{q-2l'}{l'}\osq^{2n-p-l'}\osq^{n-q-l'}(v)
  &
  \in \hlgy_{4n-p-q}(D_2(D_2))
  \end{align*}
  have the same image in $\hlgy_{4n-p-q}(D_4(V))$ under the canonical map
  \[D_2(D_2(V))\to D_4(V).\]
  \par
  In particular, if $V$ has a $2$-compatible symmetric multiplication, then
  \[
  \sum_l \binom{p-2l}{l}\sq^{2n-q-l}\sq^{n-p+l}(v)
  =
\sum_{l'}\binom{q-2l'}{l'}\sq^{2n-p-l'}\sq^{n-q-l'}(v)
\]
holds in $\hlgy_{4n-p-q}(V)$.
\end{lem}
For a proof that this implies \cref{properties:adem-relations}, see \cite{lurie-notes}*{Lecture 4}.
\begin{fact}\label{properties:groupho-fact}
  Let $p$ and $q$ be positive integers and set $\Gamma \defined (\mathfrak{S}_2\times \mathfrak{S}_2)\rtimes\mathfrak{S}_2$.
  Then the expressions
  \begin{align*}
    \sum_l \binom{p-2l}{l}\osq^{-q-l}x_{p-l}\in \hlgy_{p+q}(\mrm{B}\Gamma)
    \\
    \sum_l \binom{q-2l}{l}\osq^{-p-l'}x_{q-l'}\in \hlgy_{p+q}(\mrm{B}\Gamma)
  \end{align*}
  have the same image in $\hlgy_{p+q}(\mrm{B}\mathfrak{S}_4)$.
\end{fact}
A proof of this fact can be done using methods from group homology / Tate homology. We try to sketch it in \cref{appendixgrouphomology}.
