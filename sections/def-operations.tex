\section{Definition of the Steenrod operations}
We follow \cite{lurie-notes}*{Lecture 2} for the definitions.
%They go back to \cite{may-steenrod}.
More details can also be found in \cite{raskin-notes}*{Lecture 13}.
\begin{nonumnotation}
  We denote by $G\defined \mathfrak{S}_2 = \lset \id, \tau\rset$ the symmetric group on two elements.
\end{nonumnotation}
\begin{defn}
  Let $\chain{C}$ be a complex of $\fzg$-modules.
  Then the chain complex of its \emph{coinvariants} is defined to be the cokernel of the map
  \[
  \chain{C}\xrightarrow{\id-\tau}\chain{C}.
  \]
  We denote them by $\chain{C}_G$.
\end{defn}
\begin{construction}
\leavevmode
\begin{enumerate}
\item
  Let $M=\ff_2$ and consider it as the trivial $G$-module.
  We know that there is a free $\fz[G]$-module resolution
  \[
  \ldots
  \to
  P^{-2}
  \to
  P^{-1}
  \to
  P^{0}
  \to
  M.
  \]
  Such a resolution is for example provided by the bar complex (\cite[6.5]{weibel}).
  We set $\mrm{E}G\defined \chain{P}$.
\item
Let $\chain{V}$ be a chain complex of $\fz$-modules.
Then the tensor product $\chain{V}\tensor \chain{V}$ becomes a complex of $\fzg$-modudles:
We have
\[\left(\chain{V}\tensor\chain{V}\right)^n = \bigoplus_{i+j = n}V^i\tensor V^j,\]
and we get a $G$-action on every summand of the form
\[
\begin{tikzcd}[row sep = small]
  V^i\tensor V^j
  \ar{r}[above]{\mathrm{switch}}
  &
  V^j\tensor V^i
  \ar[hook]{r}
  &
  \cdplus{p+q = n}V^p\tensor V^q
\end{tikzcd}
\]
\item We define the \emph{homotopy coinvariants} of $\chain{V}$ to be the chain complex
\[
\left(\chain{V}\tensor \chain{V}\tensor \mrm{E}\fz\right)_G
\]
and denote them by $D_2(V)$.
For any complex $\chain{M}$ of $\fzg$-modules, the \emph{homotopy coinvariants} of $\chain{M}$
are defined as
\[
(\chain{M}\tensor \mrm{E}\fz)_G
\]
and denoted by $\hcib{M}$.
\end{enumerate}
\end{construction}
\begin{rem}
  When it comes to the homotopy coinvariants, we have the ``usual'' problem that they depend on the chosen free resolution.
  But in what follows, we only need the homology of the homotopy coinvariants of a module.
  So the natural place to perform these calculations would be the be the bounded-above derived category $\derivedd^-(\fzg\modcat)$, where we set
  \[
  C^{\bullet}\hci
  \defined
  C\dtensor_{\fzg}\fz
  \text{ and }
  D_2(V)
  \defined
  V^{\tensor 2}\hci.
  \]
  In particular, we have that the homotopy coinvariants compute group homology:
  For any $\fzg$-module $M$, it holds that
  \begin{equation}\label{def-operations:group-homology-rel}
  \hlgy_{-i}((M[0])^\bullet\hci)
  \cong
  \hlgy_i^{\mathrm{Grp}}(\fz;M)
  \end{equation}
\end{rem}
\begin{rem}
  The assignment $V\mapsto D_2(V)$ should not be regarded as a functor, since everything is only well-defined up to quasi-isomorphism anyways.
  This can be rectified by passing to the derived category, or (and this is sufficient for our purposes), by post-composing with homology.
  In other words, we get, for every $n$, a well-defined functor
  \[
  \mrm{Ch}(\fz\modcat)
  \to
  \fz\modcat,
  ~
  \chain{V}
  \mapsto
  \hlgy_n(D_2(V)).
  \]
\end{rem}
\begin{example}\label{def-operations:rpinfty-homology}
    We know that the group homology of a group $H$ computes the (singular) homology of its classifying space\footnote{I decided to differentiate between the topological classifying space and the simplicial classifying space of a group $H$, by writing $\mrm{B^t}$ for the former. Recall that the simplicial classifying space $\mrm{B}H$ of a group $H$ is the nerve of the associated one-point groupoid. These two are related in the sense that the geometric realization of $\mrm{B}H$ gives $\mrm{B^t}H$.} $\mrm{B^t}H$, i.e.
    \[
    \hlgy_i^{\mrm{Grp}}(H;M) \cong \hlgy_i^{\mrm{sing}}(\mrm{B^t}H,M).
    \]
    Using \cref{def-operations:group-homology-rel}, we get in particular that
    \[
    \hlgy_{-i}((\mrm{E}G)_G)
    =
    \hlgy_{-i}((\fz)\hci)
    \cong \hlgy_i^{\mrm{sing}}(\mrm{B^t}G,\fz)
    \] holds.
    \par
    Finally moving towards some topology, recall that we have $\mrm{B^t}G\cong \rpi$.
    So we get a description of the homotopy coinvariants of $\fz$ (We could have just computed the bar-complex of $\fz$ and then calculated the homology groups to get the same result).
    The graded module $\hlgy_{\bullet}^{\mrm{sing}}(\rpi,\fz)$ has the following description: it is given by $\fz$ in every degree $i$, with a generator that we denote by $x_i$:
    \begin{equation}\label{def-operations:rpi-generator}
      \hlgy_{\bullet}^{\mrm{sing}}(\rpi,\fz)
      \cong
      \bigoplus_{i\geq 0}
      \fz\cdot x_i.
    \end{equation}
\end{example}
\begin{defn}
  Let $\chainv$ be a chain complex.
  A \emph{symmetric multiplication} on $V$ is a chain map $\dt{V}\to \chain{V}$.
\end{defn}
\begin{example}\label{def-operations:dtwo-homology}
  Let $\fz[-n]$ be the complex with $\fz$ concentrated in degree $-n$.
  Then $\fz[-n]\tensor\fz[-n] \cong \fz[-2n]$, and $G$ acts on the whole complex.
  So we can write
  \[
  D_2(\fz[-n]) \cong
  \fz[-2n]\tensor (\mrm{E}G)_G.
  \]
  To calculate the homology of this complex, we first note that the $\fz[-2n]$ factor just induces a shift by $-2n$, and in particular, we have
  \[
  \hlgy_i(\fz[-2n]\tensor (\mrm{E}G)_G)
  \cong
  \hlgy_{i-2n}{(\mrm{E}G)_G},
  \]
  which, by \cref{def-operations:rpinfty-homology}, is isomorphic to $\hlgy_{2n-i}^{\mrm{sing}}(\rpi,\fz)$.
  Using the notation of \cref{def-operations:rpi-generator}, we write as a shorthand
  \[
  \hlgy_i(D_2(\fz[-n]))
  \cong
  \fz\cdot x_{2n-i}.
  \]
\end{example}
\begin{example}
  Let $Y$ be a topological space.
  We regard the cup product as multiplication map
  \[
  \cup\mc \singcochain(Y;\fz)\tensor \singcochain(Y;\fz)
  \to
  \singcochain(Y;\fz).
  \]
  The failure of the cup product to be commutative (in the chain complexes) is meassured by the \emph{cup-$i$-products}, which are defined as follows:
  ...
  Then the collection of cup products satisfies
  \[
  u \cup_i v - v\cup_i u
  =
  \partial(u\cup_{i+1}v) + \partial(u)\cup_{i+1}v + u\cup_{i+1}\partial(v)
  \]
  So we can view the operation $\cup_i$ as a commutative, up to a homotopy $\cup_{i+1}$.
  We can use these $\cup_i$ to construct a symmetric multiplication on $V$ as follows, by explicitly constructing the free acyclic resolution $E_{\bullet}$:
  In every degree, we set $E_i\defined \genby{e_i,\tau e_i}$, and as differential
  \[
  d e_i
  \defined
  e_{i+1} +
  (-1)^i \tau e_{i+1}.
  \]
  Then the map
  \[
  e_i \tensor u \tensor v \mapsto u \cup_{-i} v,~\tau e_i \tensor u \tensor v \mapsto v \cup_{-i} u
  \]
  is $G$-equivariant, and hence descends to a symmetric multiplication \[D_2(\singcochain(Y;\fz))\to \singcochain(V;\fz).\]
\end{example}
\begin{example}
  Let $A$ be an $E_{\infty}$-algebra over an $E_{\infty}$-operad $\oo$.
  Then $A$ has a symmetric multiplication, c.f. \cref{emspectrum:einfty-gist}.
\end{example}
\begin{construction}
  Let $V$ be a complex of $\fz$-modules, and let $v\in \hlgy_{n}(V)$ be an element.
  Then $v$ defines a map
  \begin{align*}
  \hlgy_{n}(\fz[-n])
  &
  \to
  \hlgy_{n}(V),
  \intertext{which induces maps}
  \hlgy_j(D_2(\fz[-n]))
  &
  \xrightarrow{\eta_j}
  \hlgy_j(D_2(V)).
  \end{align*}
  For $i\leq n$, we write $\overline{\sq}^i(v)$ for the image of $x_{n-i}$ under $\eta_{n+i}$.
  If $\chain{V}$ is has a symmetric multiplication $D_2(V)\to \chain{V}$, then we write $\sq^i(v)$ for the image of $\overline{\sq}^i(v)$ under the induced map
  \[
  \hlgy_{n+i}(D_2(V))
  \to
  \hlgy_{n+i}(V).
  \]
  We call the maps
  \[
  \sq^i\mc \hlgy_{\bullet}(V)\to \hlgy_{\bullet}(V)[i]
  \]
  the \emph{Steenrod operations} or \emph{Steenrod squares}.
\end{construction}
